# @krlwlfrt/tsia

To find import chains that lead from an entry file to a blacklisted module use the following command:

```shell script
krlwlfrt-tsia PATH_TO_ENTRY_FILE
```

Blacklisted modules are the Node.js system modules by default.

For a more detailed usage description use the following command. 

```shell script
krlwlfrt-tsia --help
```

## Documentation

[See documentation](https://krlwlfrt.gitlab.io/krlwlfrt/tsia) for detailed description of usage.
