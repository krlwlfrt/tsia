/*
 * Copyright (C) 2020 Karl-Philipp Wulfert
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * unknown WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Command} from 'commander';
import {dirname, join, resolve} from 'path';
import {convertCompilerOptionsFromJson} from 'typescript';
import {asyncWriteFile} from './async';
import {
  buildModuleGraph,
  findOffendingChains,
  findProjectRoot,
  getChangedFiles,
  getFileContent,
  getForbiddenModuleMap,
  INDENTATION,
  sanityCheckGraph,
} from './common';
import {Module} from 'module';

const commander = new Command('krlwlfrt-tsia');

commander
  .arguments('<entryPoint> [forbiddenModules] [maxPendingJavaScriptModules] [maxOffendingChains]')
  .action(async (entryPoint,
                 forbiddenModulesParameter,
                 maxPendingJavaScriptModulesParameter,
                 maxOffendingChainsParameter) => {
    // resolve absolute entry point
    const absoluteEntryPoint = resolve(entryPoint);

    // set default forbidden modules
    let forbiddenModules = [...Module.builtinModules];

    // use provided list of forbidden modules
    if (typeof forbiddenModulesParameter !== 'undefined') {
      forbiddenModules = forbiddenModulesParameter.split(',');
    }

    // set default max pending javascript modules
    let maxPendingJavaScriptModules = 100000;

    // use provided parameter
    if (typeof maxPendingJavaScriptModulesParameter !== 'undefined'
      && /[0-9]+/.test(maxPendingJavaScriptModulesParameter)) {
      maxPendingJavaScriptModules = parseInt(maxPendingJavaScriptModulesParameter, 10);
    }

    // set default max offending chains
    let maxOffendingChains = 1000000;

    // use provided parameter
    if (typeof maxOffendingChainsParameter !== 'undefined' && /[0-9]+/.test(maxOffendingChainsParameter)) {
      maxOffendingChains = parseInt(maxOffendingChainsParameter, 10);
    }

    console.log(`Assuming the following modules as forbidden: ${JSON.stringify(forbiddenModules, null, INDENTATION)}`);

    // find project root of absolute entry point
    const rootPath = findProjectRoot(dirname(absoluteEntryPoint));
    console.log(`Assuming root path ${rootPath}.`);

    // find changed files
    const changedFiles = await getChangedFiles(rootPath);
    console.log(`Determined ${changedFiles.length} changed file(s).`);

    // resolve path to tsconfig
    const tsConfigPath = join(rootPath.toString(), 'tsconfig.json');

    // get tsconfig
    const tsConfigJson = JSON.parse(await getFileContent(tsConfigPath));

    // parse compiler options from tsconfig
    const compilerOptionsResult = convertCompilerOptionsFromJson(tsConfigJson.compilerOptions, rootPath.toString());

    // fail on errors
    if (compilerOptionsResult.errors.length > 0) {
      console.log(compilerOptionsResult.errors);
      throw Error();
    }

    // extract compiler options from result
    const compilerOptions = compilerOptionsResult.options;
    console.log(`Successfully got compiler options from '${tsConfigPath}'.`);

    // build map of forbidden modules
    const forbiddenModuleMap = getForbiddenModuleMap(forbiddenModules);

    // build graph
    const javaScriptModuleMap = await buildModuleGraph(absoluteEntryPoint, forbiddenModuleMap, compilerOptions);
    console.log();
    console.log(`Built graph with ${Object.keys(javaScriptModuleMap).length} node(s).`);

    // sanity check graph
    const {count} = sanityCheckGraph(javaScriptModuleMap);
    console.log();
    console.log(`Sanity checked ${count} module(s).`);

    // find offending chains
    const offendingChains = findOffendingChains(
      [absoluteEntryPoint],
      forbiddenModuleMap,
      maxOffendingChains,
      maxPendingJavaScriptModules,
      changedFiles,
    );
    console.log();
    console.log(`Found ${offendingChains.length} offending chain(s).`);

    // output result
    if (offendingChains.length === 0) {
      console.log('Everything is fine!');
    } else {
      await asyncWriteFile('offendingChains.json', JSON.stringify(offendingChains, null, INDENTATION));
      console.log(`Chains: ${offendingChains.length}`);
    }
  })
  .parse(process.argv);
