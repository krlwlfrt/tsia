// /*
//  * Copyright (C) 2020 Karl-Philipp Wulfert
//  * This program is free software: you can redistribute it and/or modify it
//  * under the terms of the GNU General Public License as published by the Free
//  * Software Foundation, version 3.
//  *
//  * This program is distributed in the hope that it will be useful, but WITHOUT
//  * unknown WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
//  * more details.
//  *
//  * You should have received a copy of the GNU General Public License along with
//  * this program. If not, see <https://www.gnu.org/licenses/>.
//  */
//
// // tslint:disable-next-line:no-implicit-dependencies
// import webpack, {compilation as WebpackCompilation, Compiler, Plugin} from 'webpack';
// import {JavaScriptModuleMap} from './common';
// import CompilerHooks = webpack.compilation.CompilerHooks;
//
// /**
//  * Webpack plugin to find offending import chains
//  */
// export class FindOffendingImportChainPlugin implements Plugin {
//   /**
//    * List of absolute entry points
//    */
//   private readonly absoluteEntryPoints: string[] = [];
//
//   /**
//    * Build module graph from list of Webpack modules
//    *
//    * @param modules List of Webpack modules
//    * @param forbiddenModuleMap Map of forbidden modules
//    */
//   // tslint:disable:ban-ts-ignore
//   // tslint:disable-next-line:prefer-function-over-method
//   static buildModuleGraph(modules: WebpackCompilation.Module[],
//                           forbiddenModuleMap: JavaScriptModuleMap): JavaScriptModuleMap {
//     const javaScriptModuleMap: JavaScriptModuleMap = {};
//
//     const forbiddenModules = Object.keys(forbiddenModuleMap);
//
//     for (const module of modules) {
//       if (module.type === 'javascript/auto') {
//         // @ts-ignore
//         const path = module.resource;
//
//         if (typeof path === 'string') {
//           let javaScriptModule = javaScriptModuleMap[path];
//
//           if (typeof javaScriptModule === 'undefined') {
//             javaScriptModule = {
//               chain: [],
//               children: {},
//               parents: [],
//               path,
//             };
//             javaScriptModuleMap[javaScriptModule.path] = javaScriptModule;
//           }
//
//           // @ts-ignore
//           for (const dependency of module.dependencies as WebpackCompilation.Dependency[]) {
//             // @ts-ignore
//             const requiredModule = dependency.module as WebpackCompilation.Module;
//             // @ts-ignore
//             if (typeof requiredModule !== 'undefined' && requiredModule !== null && requiredModule.type === 'javascript/auto' && requiredModule.rawRequest !== 'tslib') {
//               // @ts-ignore
//               const childPath = requiredModule.resource;
//
//               if (typeof childPath === 'string') {
//                 let childJavaScriptModule = javaScriptModuleMap[childPath];
//
//                 if (typeof childJavaScriptModule === 'undefined') {
//                   childJavaScriptModule = {
//                     chain: [],
//                     children: {},
//                     parents: [],
//                     path: childPath,
//                   };
//                   javaScriptModuleMap[childJavaScriptModule.path] = childJavaScriptModule;
//                 }
//
//                 // @ts-ignore
//                 javaScriptModule.children[requiredModule.rawRequest] = childJavaScriptModule;
//                 childJavaScriptModule.parents.push(javaScriptModule);
//               }
//               // @ts-ignore
//             } else if (requiredModule === null && forbiddenModules.includes(dependency.request)) {
//               // @ts-ignore
//               if (!forbiddenModuleMap[dependency.request].parents.includes(javaScriptModule)) {
//                 // @ts-ignore
//                 forbiddenModuleMap[dependency.request].parents.push(javaScriptModule);
//               }
//             }
//           }
//         }
//       }
//     }
//
//     return javaScriptModuleMap;
//   }
//
//   // tslint:enable
//
//   /**
//    * Apply hooks
//    *
//    * @param compiler Compiler to apply hooks on
//    */
//   apply(compiler: Compiler) {
//     // const forbiddenModuleMap = getForbiddenModuleMap([...Module.builtinModules]);
//
//     compiler.hooks.entryOption.tap('FindOffendingImportChainPlugin', (_context, entry) => {
//       if (typeof entry === 'object') {
//         for (const identifier in entry) {
//           if (!entry.hasOwnProperty(identifier)) {
//             continue;
//           }
//
//           for (const path of entry[identifier]) {
//             if (!this.absoluteEntryPoints.includes(path)) {
//               this.absoluteEntryPoints.push(path);
//             }
//           }
//         }
//       }
//
//       console.log(this.absoluteEntryPoints);
//     });
//
//     const hooks: Array<keyof CompilerHooks> = [
//       'afterPlugins',
//       'afterResolvers',
//       'environment',
//       'afterEnvironment',
//       'beforeRun',
//       'run',
//       'watchRun',
//       'normalModuleFactory',
//       'contextModuleFactory',
//       'beforeCompile',
//       'compile',
//       'thisCompilation',
//       'compilation',
//       'make',
//       'afterCompile',
//       'shouldEmit',
//       'emit',
//       'afterEmit',
//       // 'assetEmitted',
//       'done',
//       'failed',
//       'invalid',
//       'watchClose',
//       // 'infrastructureLog',
//       // 'log',
//     ];
//
//     for (const hook of hooks) {
//       compiler.hooks[hook].tap('FindOffendingImportChainPlugin', () => {
//         console.log(`HOOK: ${hook} was reached!`);
//       });
//     }
//
//     /*
//     compiler.hooks.compilation.tap('FindOffendingImportChainPlugin', (compilation) => {
//       compilation.hooks.afterOptimizeChunkAssets.tap('FindOffendingImportChainPlugin', (chunks) => {
//         const modules: WebpackCompilation.Module[] = [];
//
//         for (const chunk of chunks) {
//           modules.push.apply(modules, chunk.getModules());
//         }
//
//         const javaScriptModuleMap = FindOffendingImportChainPlugin.buildModuleGraph(modules, forbiddenModuleMap);
//         sanityCheckGraph(javaScriptModuleMap);
//
//         console.log();
//         console.log('afterOptimizeChunkAssets', modules.length);
//         console.log();
//       });
//
//       compilation.hooks.afterOptimizeDependencies.tap('FindOffendingImportChainPlugin', (modules) => {
//         const forbiddenModuleMap = getforbiddenModuleMap([...Module.builtinModules]);
//
//         const javaScriptModuleMap = FindOffendingImportChainPlugin.buildModuleGraph(modules, forbiddenModuleMap);
//
//         console.log(sanityCheckGraph(javaScriptModuleMap));
//
//         tslint:disable-next-line:no-magic-numbers
//         const offendingChains = findOffendingChains(this.absoluteEntryPoints, forbiddenModuleMap, 10000, 1000000, []);
//
//         console.log(offendingChains);
//       });
//     });
//      */
//   }
// }
