/*
 * Copyright (C) 2020 Karl-Philipp Wulfert
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * unknown WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {execSync} from 'child_process';
import {existsSync, lstatSync, PathLike} from 'fs';
import {basename, dirname, join} from 'path';
import {stdout} from 'single-line-log';
import {
  CompilerOptions,
  createCompilerHost,
  createSourceFile,
  isCallExpression,
  isExportDeclaration,
  isExpressionStatement,
  isIdentifier,
  isImportDeclaration,
  isStringLiteral,
  isVariableStatement,
  resolveModuleName,
  ScriptTarget,
  SyntaxKind,
} from 'typescript';
import {asyncReadFile} from './async';

/**
 * Maximum nested directory depth
 */
export const MAX_NESTED_DIRECTORY_DEPTH = 10000;

/**
 * Number of spaces to indent code
 */
export const INDENTATION = 2;

/**
 * A JavaScript module
 */
export interface JavaScriptModule {
  /**
   * Chain to root
   */
  chain: string[];
  /**
   * Map of imported/required modules by reference
   */
  children: JavaScriptModuleMap;
  /**
   * List of importing/requiring modules
   */
  parents: JavaScriptModule[];
  /**
   * Absolute path
   */
  path: string;
}

/**
 * Map of JavaScript modules either by path or reference
 */
export interface JavaScriptModuleMap {
  [pathOrReference: string]: JavaScriptModule;
}

/**
 * Find root of project
 *
 * @param path Path to a project or a file in a project
 * @param requireTsConfig Whether or not a tsconfig.json is required to qualify as a project
 */
export function findProjectRoot(path: PathLike, requireTsConfig = true): string {
  let _path = path.toString();

  let i = 0;

  do {
    if (_path === '/') {
      break;
    }

    if (existsSync(join(_path, 'package.json'))
      && (existsSync(join(_path, 'tsconfig.json')) || !requireTsConfig)) {
      break;
    }

    _path = join(_path, '..');
  } while (i++ < MAX_NESTED_DIRECTORY_DEPTH);

  return _path;
}

/**
 * Get content of file
 *
 * @param path Path to file of which to get the content of
 */
export async function getFileContent(path: PathLike): Promise<string> {
  const buffer = await asyncReadFile(path);

  return buffer.toString();
}

/**
 * Get module references from file
 *
 * @param path Path to file of which to get module references from
 * @param compilerOptions TypeScript compiler options
 */
export async function getModuleReferences(path: string, compilerOptions: CompilerOptions): Promise<string[]> {
  const moduleReferences: string[] = [];

  const content = await getFileContent(path);

  const sourceFile = createSourceFile(path, content, compilerOptions.target as ScriptTarget);

  const firstChild = sourceFile.getChildren()[0];
  const children = firstChild.getChildren();

  for (const node of children) {
    if (isImportDeclaration(node)) {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      moduleReferences.push((node.moduleSpecifier as any).text);
    } else if (isExportDeclaration(node)) {
      if (typeof node.moduleSpecifier !== 'undefined') {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        moduleReferences.push((node.moduleSpecifier as any).text);
      }
    } else if (isVariableStatement(node)) {
      if (typeof node.declarationList !== 'undefined'
        && Array.isArray(node.declarationList.declarations)
        && node.declarationList.declarations.length > 0) {
        const initializer = node.declarationList.declarations[0].initializer;

        if (typeof initializer !== 'undefined'
          && typeof initializer.expression !== 'undefined'
          && initializer.expression.originalKeywordKind === SyntaxKind.RequireKeyword) {
          moduleReferences.push(initializer.arguments[0].text);
        }
      }
    } else if (isExpressionStatement(node) && isCallExpression(node.expression)) {
      for (const argument of node.expression.arguments) {
        if (isCallExpression(argument) && isIdentifier(argument.expression)) {
          if (argument.expression.escapedText === 'require' && isStringLiteral(argument.arguments[0])) {
            moduleReferences.push(argument.arguments[0].text);
          }
        }
      }
      // } else if (isImportEqualsDeclaration(node)) {
      // TODO
      // } else if (isExternalModuleReference(node)) {
      // TODO
    }
  }

  return moduleReferences;
}

/**
 * Build module graph
 *
 * @param absoluteEntryPoint Absolute entry point
 * @param forbiddenModuleMap Map of forbidden modules
 * @param compilerOptions TypeScript compiler options
 */
export async function buildModuleGraph(absoluteEntryPoint: string,
                                       forbiddenModuleMap: JavaScriptModuleMap,
                                       compilerOptions: CompilerOptions): Promise<JavaScriptModuleMap> {
  const javaScriptModuleMap: JavaScriptModuleMap = {};
  const pendingJavaScriptModules: JavaScriptModule[] = [];

  const forbiddenModules = Object.keys(forbiddenModuleMap);

  const compilerHost = createCompilerHost(compilerOptions);

  pendingJavaScriptModules.push({
    chain: [],
    children: {},
    parents: [],
    path: absoluteEntryPoint,
  });

  while (pendingJavaScriptModules.length > 0) {
    const javaScriptModule = pendingJavaScriptModules.shift() as JavaScriptModule;

    javaScriptModuleMap[javaScriptModule.path] = javaScriptModule;

    for (const moduleReference of await getModuleReferences(javaScriptModule.path, compilerOptions)) {
      if (forbiddenModules.includes(moduleReference)) {
        if (!forbiddenModuleMap[moduleReference].parents.includes(javaScriptModule)) {
          forbiddenModuleMap[moduleReference].parents.push(javaScriptModule);
        }
        continue;
      }

      const moduleResolutionResult = resolveModuleName(
        moduleReference,
        javaScriptModule.path,
        compilerOptions,
        compilerHost,
      );

      if (typeof moduleResolutionResult.resolvedModule === 'undefined') {
        // TODO
        // module not found
        continue;
      }

      let resolvedFileName = moduleResolutionResult.resolvedModule.resolvedFileName;

      // use transpiled JavaScript file instead of TypeScript declaration if it exists
      if (/d\.ts$/.test(resolvedFileName) && existsSync(join(dirname(resolvedFileName), `${basename(resolvedFileName, '.d.ts')}.js`))) {
        resolvedFileName = join(dirname(resolvedFileName), `${basename(resolvedFileName, '.d.ts')}.js`);
      }

      if (resolvedFileName.includes('@types')) {
        const resolvedFileNameRootPath = join(findProjectRoot(resolvedFileName, false), '..', '..', moduleReference);
        const packageJson = JSON.parse(await getFileContent(join(resolvedFileNameRootPath, 'package.json')));

        if (typeof packageJson.main === 'string') {
          resolvedFileName = join(resolvedFileNameRootPath, packageJson.main);
        }
      } else if (resolvedFileName.includes('node_modules')) {
        const resolvedFileNameRootPath = findProjectRoot(resolvedFileName, false);
        const packageJson = JSON.parse(await getFileContent(join(resolvedFileNameRootPath, 'package.json')));

        if (typeof packageJson.browser === 'string' && moduleReference === packageJson.name) {
          resolvedFileName = join(resolvedFileNameRootPath, packageJson.browser);
        } else if (typeof packageJson.browser === 'object') {
          for (const mappedPath in packageJson.browser) {
            if (!Object.prototype.hasOwnProperty.call(packageJson.browser, mappedPath)) {
              continue;
            }

            if (join(resolvedFileNameRootPath, mappedPath) === resolvedFileName) {
              resolvedFileName = join(resolvedFileNameRootPath, packageJson.browser[mappedPath]);
              break;
            }
          }
        }
      }

      if ((!existsSync(resolvedFileName) || lstatSync(resolvedFileName)
        .isDirectory())) {
        if (existsSync(`${resolvedFileName}.ts`)) {
          resolvedFileName += '.ts';
        } else {
          resolvedFileName += '.js';
        }
      }

      let importedJavaScriptModule = javaScriptModuleMap[resolvedFileName];

      if (typeof importedJavaScriptModule === 'undefined') {
        const alreadyPendingJavaScriptModule = pendingJavaScriptModules.find((pendingJavaScriptModule) => {
          return pendingJavaScriptModule.path === resolvedFileName;
        });

        if (typeof alreadyPendingJavaScriptModule !== 'undefined') {
          importedJavaScriptModule = alreadyPendingJavaScriptModule;
        }
      }

      if (typeof importedJavaScriptModule !== 'undefined') {
        if (!importedJavaScriptModule.parents.includes(javaScriptModule)) {
          importedJavaScriptModule.parents.push(javaScriptModule);
        }
      } else {
        importedJavaScriptModule = {
          chain: [],
          children: {},
          parents: [
            javaScriptModule,
          ],
          path: resolvedFileName,
        };
        pendingJavaScriptModules.push(importedJavaScriptModule);
      }
      javaScriptModule.children[moduleReference] = importedJavaScriptModule;
    }

    stdout(`Completed: ${Object.keys(javaScriptModuleMap).length}\nPending: ${pendingJavaScriptModules.length}`);
  }

  stdout.clear();

  return javaScriptModuleMap;
}

/**
 * Sanity check that no module requires itself or is required by itself
 *
 * @param javaScriptModuleMap Map of JavaScript modules
 */
export function sanityCheckGraph(javaScriptModuleMap: JavaScriptModuleMap): {
  /**
   * Count of checked modules
   */
  count: number;
  /**
   * Whether or not the graph is sane
   */
  sane: boolean;
} {
  let count = 0;

  for (const path in javaScriptModuleMap) {
    if (!Object.prototype.hasOwnProperty.call(javaScriptModuleMap, path)) {
      continue;
    }

    const javaScriptModule = javaScriptModuleMap[path];

    for (const parentJavaScriptModule of javaScriptModule.parents) {
      if (parentJavaScriptModule.path === javaScriptModule.path) {
        return {
          count,
          sane: false,
        };
      }
    }

    for (const reference in javaScriptModule.children) {
      if (!Object.prototype.hasOwnProperty.call(javaScriptModule.children, reference)) {
        continue;
      }

      const childJavaScriptModule = javaScriptModule.children[reference];
      if (childJavaScriptModule.path === javaScriptModule.path) {
        return {
          count,
          sane: false,
        };
      }
    }

    count++;
  }

  return {
    count,
    sane: true,
  };
}

/**
 * Get a list of changed files
 *
 * @param rootPath Root path of project
 */
export async function getChangedFiles(rootPath: string): Promise<string[]> {
  const changedFiles: string[] = [];

  const buffer = execSync('git status --porcelain', {
    cwd: rootPath,
  });
  const output = buffer.toString();

  for (let line of output.split('\n')) {
    if (line.indexOf('fatal:') === 0) {
      return changedFiles;
    }

    line = line.trim();
    const [status, filePath] = line.split(' ');

    if (['M', '??'].includes(status)) {
      changedFiles.push(join(rootPath, filePath));
    }
  }

  return changedFiles;
}

/**
 * Find offending chains
 *
 * @param absoluteEntryPoints Absolute entry point
 * @param forbiddenModuleMap Map of forbidden modules
 * @param maxOffendingChains Maximum number of offending hcains
 * @param maxPendingJavaScriptModules Maximum number of pending JavaScript modules
 * @param relevantPaths List of relevant paths
 */
export function findOffendingChains(absoluteEntryPoints: string[],
                                    forbiddenModuleMap: JavaScriptModuleMap,
                                    maxOffendingChains: number,
                                    maxPendingJavaScriptModules: number,
                                    relevantPaths: string[]): string[][] {
  const offendingChains: string[][] = [];

  const pendingJavaScriptModules: JavaScriptModule[] = [];
  for (const reference in forbiddenModuleMap) {
    if (!Object.prototype.hasOwnProperty.call(forbiddenModuleMap, reference)) {
      continue;
    }

    const javaScriptModule = forbiddenModuleMap[reference];

    if (javaScriptModule.parents.length > 0) {
      pendingJavaScriptModules.push(javaScriptModule);
    }
  }

  if (pendingJavaScriptModules.length === 0) {
    return offendingChains;
  }

  const chainPartsToRoot: { [path: string]: string[]; } = {};

  while (pendingJavaScriptModules.length > 0
  && offendingChains.length < maxOffendingChains
  && pendingJavaScriptModules.length < maxPendingJavaScriptModules) {
    const javaScriptModule = pendingJavaScriptModules.shift() as JavaScriptModule;

    // found a chain
    if (absoluteEntryPoints.includes(javaScriptModule.path)) {
      const offendingChain = [
        ...javaScriptModule.chain,
      ];
      offendingChain.push(javaScriptModule.path);
      offendingChains.push(offendingChain);

      // cache already found chain parts
      // eslint-disable-next-line no-magic-numbers
      for (let i = 0; i < offendingChain.length - 1; i++) {
        const chainPartToRoot = offendingChain.slice(i);
        chainPartsToRoot[chainPartToRoot[0]] = chainPartToRoot;
      }

      continue;
    }

    for (const parentJavaScriptModule of javaScriptModule.parents) {
      if (parentJavaScriptModule.chain.includes(javaScriptModule.path)) {
        // ignore cycle
        continue;
      }

      const clonedJavaScriptModule = {
        ...parentJavaScriptModule,
      };
      clonedJavaScriptModule.chain = [...javaScriptModule.chain];
      clonedJavaScriptModule.chain.push(javaScriptModule.path);

      // use already found chain part
      if (typeof chainPartsToRoot[clonedJavaScriptModule.path] !== 'undefined') {
        clonedJavaScriptModule.chain.push(...chainPartsToRoot[clonedJavaScriptModule.path]);
        offendingChains.push(clonedJavaScriptModule.chain);

        continue;
      }

      pendingJavaScriptModules.push(clonedJavaScriptModule);

      stdout(`Chains: ${offendingChains.length}\nPending: ${pendingJavaScriptModules.length}\nCached chain parts: ${Object.keys(chainPartsToRoot).length}`);
    }

    if (relevantPaths.length === 0) {
      continue;
    }

    for (const offendingChain of offendingChains) {
      for (const path of offendingChain) {
        if (relevantPaths.includes(path)) {
          stdout.clear();
          console.log();
          console.log();
          console.log(`Offending chain includes relevant path ${path}!`);
          console.log();
          for (const pathInChain of [...offendingChain].reverse()) {
            if (pathInChain === path) {
              console.log(`     >>>>>>>>>> ${pathInChain} <<<<<<<<<<`);
            } else {
              console.log(`${pathInChain}`);
            }
          }
          console.log();

          return offendingChains;
        }
      }
    }
  }

  stdout.clear();

  offendingChains.forEach((offendingChain) => offendingChain.reverse());

  return offendingChains;
}

/**
 * Get map of forbidden modules
 *
 * @param forbiddenModules List of forbidden modules
 */
export function getForbiddenModuleMap(forbiddenModules: string[]): JavaScriptModuleMap {
  const forbiddenModuleMap: { [reference: string]: JavaScriptModule; } = {};

  for (const forbiddenModule of forbiddenModules) {
    forbiddenModuleMap[forbiddenModule] = {
      chain: [],
      children: {},
      parents: [],
      path: forbiddenModule,
    };
  }

  return forbiddenModuleMap;
}
