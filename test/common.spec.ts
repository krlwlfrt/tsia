/*
 * Copyright (C) 2020 Karl-Philipp Wulfert
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * unknown WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {params, suite, timeout} from '@testdeck/mocha';
import {findProjectRoot, getModuleReferences} from '../src/common';
import {getDefaultCompilerOptions} from 'typescript';
import {join} from 'path';
import {deepStrictEqual, strictEqual} from 'assert';

@suite('CommonSpec') @timeout(600000)
export class CommonSpec {
  @params({
    file: __filename,
    moduleReferences: [
      '@testdeck/mocha',
      '../src/common',
      'typescript',
      'path',
      'assert',
    ],
  })
  @params({
    file: join(__dirname, 'resources', 'common.dummy.js'),
    moduleReferences: [
      'first',
      'second',
      'third',
      'var-require',
    ],
  })
  @params.naming((param: { file: string, moduleReferences: string[] }) => `get module references of ${param.file}`)
  async 'get module references'(param: { file: string, moduleReferences: string[] }) {
    const moduleReferences = await getModuleReferences(param.file, getDefaultCompilerOptions());

    strictEqual(moduleReferences.length, param.moduleReferences.length);

    deepStrictEqual(moduleReferences, param.moduleReferences);
  }

  @params({
    file: join(__dirname, 'resources', 'common.dummy.js'),
    requireTsConfig: true,
    root: join(__dirname, '..'),
  })
  @params({
    file: join(__dirname, 'resources', 'common.dummy.js'),
    requireTsConfig: true,
    root: join(__dirname, '..'),
  })
  @params({
    file: '/home',
    requireTsConfig: true,
    root: '/',
  })
  @params.naming((param: { file: string; requireTsConfig: boolean; root: string }) => `find root of ${param.file}`)
  'find root'(param: { file: string; requireTsConfig: boolean; root: string }) {
    strictEqual(findProjectRoot(param.file, param.requireTsConfig), param.root);
  }
}
