function foo() {

}

foo(require('first'), require('second'), require('third'));
var module1 = require('var-require');

(() => {
  require('iife');
  var module2 = require('iife-var-require');
})();

(() => {
  require('iife-not-executed-require');
  var module3 = require('iife-not-executed-var-require');
});

if (0 === 1) {
  var module4 = require('iife-not-executed-var-require');
}
